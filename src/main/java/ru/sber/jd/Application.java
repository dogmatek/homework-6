package ru.sber.jd;

import ru.sber.jd.dto.ApartmentDto;
import ru.sber.jd.services.Price;
import ru.sber.jd.services.PriceProcessor;

import static ru.sber.jd.dto.StateDto.BUILD;
import static ru.sber.jd.dto.StateDto.EURO;

public class Application {
    public static void main(String[] args) {
        Price statePrice = new PriceProcessor();

        ApartmentDto apartment = new ApartmentDto();
        apartment.setType("Офис");
        apartment.setArea(15.5);
        apartment.setState(EURO);
        apartment.setFloor(5);


       // PriceTypeDaoImp.getPriceType

        System.out.println(apartment);
        try {
            System.out.println("Стоимость квартиры: " + statePrice.calculatePrice(apartment));

        }catch (TypeNotPresentException ex) {
            System.out.println("Отловлена ошибка: " + ex);
        } catch (RuntimeException e) {
            System.out.println("Отловлена ошибка: " + e);
        }

    }
}
