package ru.sber.jd.services;

import lombok.ToString;
import ru.sber.jd.dto.ApartmentDto;
import ru.sber.jd.dto.StateDto;
import ru.sber.jd.dto.TypePriceDto;
import ru.sber.jd.exceptions.TypeNotFoundExeption;
import ru.sber.jd.repositories.PriceTypeDaoImpl;

public class PriceProcessor implements Price {


    public Integer calculatePrice(ApartmentDto apartment) {
        Integer price = null;
        // Присваиваем стоимость за 1 кв.м в зависимости от типа квартиры
        PriceTypeDaoImpl priceTypeDaoImpl = new PriceTypeDaoImpl() ;
        TypePriceDto[] typePriceDtos = priceTypeDaoImpl.getPriceType();
        for (TypePriceDto typePriceDto : typePriceDtos) {
            if(apartment.getType().equals(typePriceDto.getType())){
                price = typePriceDto.getPrice();
                break;
            }
        }
        if(price == null){
            //TODO: Исключение тип квартиры
            throw new TypeNotFoundExeption(String.format("В БД отсутствует тип квартиры:\"%s\" ", apartment.getType()));
        }

        if(apartment.getArea() < 30) {
            //TODO: Исключение несоответствие площади
            throw new RuntimeException(String.format("Минимальная площадь квартиры - %.2f м2 не соответствует нормативу по площади", apartment.getArea()));
        }

        //Высчитываем стоимость квартиры с учетом её площади и корректировки на состояние отделки
        price = (int)(  price.doubleValue()
                        * apartment.getArea()
                        * (1.0 + apartment.getState().getCorrection())
                    );

        return price;
    }
}
