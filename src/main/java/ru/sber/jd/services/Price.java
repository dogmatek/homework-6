package ru.sber.jd.services;

import ru.sber.jd.dto.ApartmentDto;

public interface Price {
    Integer calculatePrice(ApartmentDto apartment) ;
}
