package ru.sber.jd.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class TypePriceDto {
    private String type;
    private Integer price;
}
