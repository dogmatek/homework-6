package ru.sber.jd.dto;

import lombok.Data;

import java.security.acl.Owner;

/*
    Класс квартира
 */
@Data
public class ApartmentDto {
    private StateDto state;
    private Integer floor;
    private Double area;
    private String type;
}
