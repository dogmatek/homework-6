package ru.sber.jd.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


/*
@RequiredArgsConstructor - создаёт конструктор с требуемыми аргументами, где обязательными являются
окончательные поля и поля с аннотацией @NonNull
 */

    /*
    Сотсоотяние объекта недвижимости
     */
@Getter
@RequiredArgsConstructor
public enum StateDto {
    BUILD ("Без ремонта (Состояние от застройщика)", 0.0),
    NORMAL ("Простой ремонт", 0.01),
    EURO ( "Евроремонт", 0.1);


    private final String name;
    // Корректирующий коэффициент
    private final Double correction;
}

