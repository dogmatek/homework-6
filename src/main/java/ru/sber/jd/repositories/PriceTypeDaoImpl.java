package ru.sber.jd.repositories;


import ru.sber.jd.dto.TypePriceDto;

public class PriceTypeDaoImpl implements PriceTypeDao {

    public TypePriceDto[] getPriceType() {
        return new TypePriceDto[] {
                TypePriceDto.builder().type("Студия").price(58000).build(),
                TypePriceDto.builder().type("1-комнатная квартира").price(60000).build(),
                TypePriceDto.builder().type("2-комнатная квартира").price(55000).build(),
                TypePriceDto.builder().type("3-комнатная квартира").price(53000).build(),
                TypePriceDto.builder().type("4-комнатная квартира").price(50000).build()
        };
    }
}
