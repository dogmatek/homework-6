package ru.sber.jd.repositories;

import ru.sber.jd.dto.TypePriceDto;

public interface PriceTypeDao {
    TypePriceDto[] getPriceType();
}
