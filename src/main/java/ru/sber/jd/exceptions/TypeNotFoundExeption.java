package ru.sber.jd.exceptions;

public class TypeNotFoundExeption extends  RuntimeException{

    public TypeNotFoundExeption(String message){
        super(message);
    }

}
